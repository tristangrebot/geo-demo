#!/bin/sh

set -e

# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

# Go to home folder and create tmp folder
cd ~
mkdir tmp
cd tmp

# Download IRIS shape file and extract it
wget -N -O CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z ftp://Contours_IRIS_ext:ao6Phu5ohJ4jaeji@ftp3.ign.fr/CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z.001
7z x CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z
rm CONTOURS-IRIS_2-1__SHP__FRA_2019-01-01.7z

shp2pgsql -s 2154:4326 CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139/CONTOURS-IRIS_2-1_SHP_LAMB93_FXX-2019/CONTOURS-IRIS.shp geolab.iris_2019 | psql -d geo_db
shp2pgsql -s 5490:4326 -a CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139/CONTOURS-IRIS_2-1_SHP_RGAF09UTM20_GLP-2019/CONTOURS-IRIS.shp geolab.iris_2019 | psql -d geo_db
shp2pgsql -s 5490:4326 -a CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139/CONTOURS-IRIS_2-1_SHP_RGAF09UTM20_MTQ-2019/CONTOURS-IRIS.shp geolab.iris_2019 | psql -d geo_db
shp2pgsql -s 4471:4326 -a CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139/CONTOURS-IRIS_2-1_SHP_RGM04UTM38S_MYT-2019/CONTOURS-IRIS.shp geolab.iris_2019 | psql -d geo_db
shp2pgsql -s 2975:4326 -a CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139/CONTOURS-IRIS_2-1_SHP_RGR92UTM40S_REU-2019/CONTOURS-IRIS.shp geolab.iris_2019 | psql -d geo_db
shp2pgsql -s 2972:4326 -a CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01/CONTOURS-IRIS/1_DONNEES_LIVRAISON_2020-01-00139/CONTOURS-IRIS_2-1_SHP_UTM22RGFG95_GUF-2019/CONTOURS-IRIS.shp geolab.iris_2019 | psql -d geo_db

rm -rf CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01

# Téléchargement des contours administratifs français

# Régions
wget -N -O regions-20180101-shp.zip https://osm13.openstreetmap.fr/~cquest/openfla/export/regions-20180101-shp.zip
7z x regions-20180101-shp.zip -oregions
rm regions-20180101-shp.zip

shp2pgsql -s 4326 regions/regions-20180101.shp geolab.regions_2018 | psql -d geo_db

# Départements
wget -N -O departements-20180101-shp.zip https://osm13.openstreetmap.fr/~cquest/openfla/export/departements-20180101-shp.zip
7z x departements-20180101-shp.zip -odepartements
rm departements-20180101-shp.zip

shp2pgsql -s 4326 departements/departements-20180101.shp geolab.departements_2018 | psql -d geo_db

# Communes
wget -N -O communes-20200101-shp.zip https://osm13.openstreetmap.fr/~cquest/openfla/export/communes-20200101-shp.zip
7z x communes-20200101-shp.zip -ocommunes
rm communes-20200101-shp.zip

shp2pgsql -s 4326 communes/communes-20200101.shp geolab.communes_2020 | psql -d geo_db
