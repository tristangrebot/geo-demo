#!/bin/sh

set -e

# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

# Create the 'template_postgis' template db
psql --username="$PGUSER" --dbname="$POSTGRES_DB"  <<-'EOSQL'
CREATE EXTENSION hstore;
ALTER TABLE geometry_columns OWNER TO geo_user;
ALTER TABLE spatial_ref_sys OWNER TO geo_user;
CREATE SCHEMA geolab;
EOSQL
